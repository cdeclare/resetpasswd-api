using System;


namespace ResetPasswdApi.Models
{
    public class ReturnType 
    {
        public string RetCode { get; set; } = "200";
        public string RetMsg { get; set; }
    }

    public class PasswdType 
    {
        public string Password { get; set; }
        public string NewPassword { get; set; }
    }

    public class EMailType 
    {
        public string EMail { get; set; }
        public string URL { get; set; }
    }
}