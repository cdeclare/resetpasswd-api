using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ResetPasswdApi.Models;
using System.Security.Cryptography;

namespace ResetPasswdApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ResetPasswdController : ControllerBase
	{
		// GET api/values
		[HttpGet]
		public string Get()
		{
			return string.Format("Alive : {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
		}

		// GET api/values/5
		[HttpGet("{id}/{hash}")]
		public ActionResult<string> Get(string id, string hash)
		{
			string RetMsg = "";
			string Msg;

			string HashDate = GetHash(id);
			if (HashDate == hash) {
				if (ResetPasswd(id, out Msg)) {
					RetMsg = "초기 패스워드 메일 전송";
				}
				else {
					RetMsg = string.Format("ERROR : {0}", Msg);
				}
			}
			else {
				RetMsg = "ERROR : URL이 올바르지 않습니다.";
			}
			
			return RetMsg;
		}

		[HttpPost("{id}")]
		public ReturnType Post(string id, PasswdType passwd)
		{
			ReturnType ret = new ReturnType();
			string Msg;

			bool bSuccess = ChangePasswd(id, passwd.Password, passwd.NewPassword, out Msg);
			if (!bSuccess) {
				ret.RetCode = "400";
				ret.RetMsg = Msg;
			}

			return ret;
		}

		[HttpDelete("{id}")]
		public ReturnType Delete(string id, EMailType email)
		{
			ReturnType ret = new ReturnType();
			string Email = GetEMailAddress(id);
			
			if (string.IsNullOrEmpty(Email)) {
				ret.RetCode = "400";
				ret.RetMsg = "EMAIL ADDRESS IS NOT SET";
			}
			else if (Email != email.EMail) {
				ret.RetCode = "400";
				ret.RetMsg = "EMAIL ADDRESS IS NOT CORRECT";
			}
			else {
				// string url = "http://localhost:5000/api/resetpasswd";
				string url = email.URL;

				string callback = string.Format("{0}/{1}/{2}", url, id, GetHash(id));
				SendReplyMail(id, Email, callback);
				ret.RetMsg = Email;
			}

			return ret;
		}

		private static void SendReplyMail(string account, string addr, string callback)
		{
			SmtpClient smtp = new SmtpClient("imail1.interpark.com");

			MailMessage msg = new MailMessage();
			msg.From = new MailAddress("ldap@interpark.com");
			msg.To.Add(addr);
			msg.Subject = string.Format("[LDAP] {0} 계정 패스워드 초기화 URL 전송", account);
			msg.Body = string.Format(
				@"PASSWORD 초기화 링크 <br/><br/> 
				<a href='{0}' target='_blank'>{0}</a>
				", callback);
			msg.IsBodyHtml = true;
			smtp.Send(msg);
		}

		private static void SendNoticeMail(string addr, string account, string passwd)
		{
			SmtpClient smtp = new SmtpClient("imail1.interpark.com");

			MailMessage msg = new MailMessage();
			msg.From = new MailAddress("ldap@interpark.com");
			msg.To.Add(addr);
			msg.Subject = string.Format("[LDAP] {0} 계정 패스워드 초기화 ", account);
			msg.Body = string.Format(
				@"패스워드가 아래의 문자로 초기화 되었습니다. <br/><br/> 
				PASSWORD : {0}
				", passwd);
			msg.IsBodyHtml = true;
			smtp.Send(msg);
		}
		private static string GetHash(string account) 
		{
			string today = DateTime.Now.ToString("yyyyMMdd");
			string str = account + today;

			var hash = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(str));
			return string.Concat(hash.Select(b => b.ToString("x2")));
		}
		
		private static string CreatePassword(int length, bool special)
		{
			string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

			if (special)
			{
				valid = "!@#$";
			}

			StringBuilder res = new StringBuilder();
			Random rnd = new Random();
			while (0 < length--)
			{
				res.Append(valid[rnd.Next(valid.Length)]);
			}
			return res.ToString();
		}

		private static string CreatePassword()
		{
			string passwd = CreatePassword(9, false);
			passwd += CreatePassword(1, true);
			return passwd;
		}

		private static bool ChangePasswd(string userName, string passwd, string newpasswd, out string message)
		{
			bool bSuccess = false;
			message = "";
			try
			{
				using (var context = new PrincipalContext(ContextType.Domain))
				{
					using (var user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName))
					{
						user.ChangePassword(passwd, newpasswd);
						user.Save();
					}
				}
				bSuccess = true;
			}
			catch (Exception ex)
			{
				message = ex.Message;
			}

			return bSuccess;
		}

		private static bool ResetPasswd(string userName, out string message)
		{
			bool bSuccess = false;
			message = "";
			try
			{
				using (var context = new PrincipalContext(ContextType.Domain))
				{
					using (var user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName))
					{
						string passwd = CreatePassword();

						user.SetPassword(passwd);
						user.Save();

						string addr = string.Format("{0}<{1}>", user.GivenName, user.EmailAddress);
						SendNoticeMail(addr, userName, passwd);
					}
				}
				bSuccess = true;
			}
			catch (Exception ex)
			{
				message = ex.Message;
			}

			return bSuccess;
		}

		private static string GetEMailAddress(string userName)
		{
			string retval = "";
			try
			{
				using (var context = new PrincipalContext(ContextType.Domain))
				{
					using (var user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName))
					{
						retval = user.EmailAddress;
					}
				}
			}
			catch (Exception ex)
			{
				retval = ex.Message;
			}

			return retval;
		}

		
	}
}
