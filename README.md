
## resetpasswd-api 
LDAP(Active Directory) 패스워드 변경 및 초기화 툴 

### 1. Requirements
- ddotnetcore-sdk
- dotnetcore-windowshosting (IIS Hosting)

```bat
choco install dotnetcore-sdk
choco install dotnetcore-windowshosting
```

### 2. Run, Build, Publish 

```bat
REM Run
dotnet run --urls=http://0.0.0.0:5000

REM build 
dotnet build 

REM publish 
dotnet publish -c Release -o bin\Release
```

- Publish.bat : Deploy
```bat
REM recycle apppool
%systemroot%\system32\inetsrv\appcmd recycle apppool /apppool.name:resetpasswd

REM 배포용 
dotnet publish -c Release -o D:\Web\resetpasswd

REM recycle apppool
%systemroot%\system32\inetsrv\appcmd recycle apppool /apppool.name:resetpasswd

REM test 
powershell Invoke-RestMethod -Uri https://ad.inpark.kr:8443/api/resetpasswd/
```

### 3. IIS Hosting

#### API 배포 
- .NET CLR 버전 → 관리코드 없음
- 관리되는 파이프라인 모드 → 통합
- 이슈 
	- SetPassword 함수(PrincipalContext)의 경우 기본 app pool 계정으로 실행 할 경우 동작하지 않음 (아래 2가지 중 하나 선택)
		1. App pool 실행계정 변경
		2. PrincipalContext 객체의 생성시 필요 권한의 계정으로 생성  

#### WebPage 배포 
- WebPage 디렉토리를 서비스 디렉토리로 복사 
- Static Page Service

